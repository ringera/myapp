import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MuiAccordion from '@material-ui/core/Accordion';
import {FormGroup,FormControl,FormHelperText,FormLabel,TextField,Button} from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import CustomizedSwitches from './CustomizedSwitches';
const Accordion = withStyles({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: 'rgba(0, 0, 0, .03)',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiAccordionDetails);

export default function CustomizedAccordions() {
  const [expanded, setExpanded] = React.useState('panel1');

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };
  const [state, setState] = React.useState({
    move: false,
    teeth: false,
    culling: false,
  });

  const handleSwitchChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  return (
    <div>
      <FormControl component="fieldset">
      <FormGroup>
        <Accordion  expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
          <AccordionSummary aria-controls="panel1d-content" id="panel1d-header" >
            <Typography  >
            <Grid >
            
      
      
          <Typography component="div">
          <Grid component="label" container alignItems="right" spacing={1}>
            
            <Grid item><FormLabel component="legend">Move this Animal?</FormLabel></Grid>
            <Grid item>
            <Switch checked={state.move} onChange={handleSwitchChange} name="move" />
            </Grid>
          
          </Grid>
        </Typography>
      
        
      
          </Grid>

            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography >
            <div  xs={12} >
      <h5>RFID SCAN</h5>
   
      <TextField
        label="RFID Scan"
        name="ID"
      
        margin="normal"
        variant="outlined"
        autoComplete="off"
        fullWidth
      />
      <Button
        variant="contained"
        fullWidth
        color="primary"
        style={{ marginTop: "1rem" }}
       // onClick={() => navigation.next()}
      >
        Next
      </Button>
    </div>
            </Typography>
          </AccordionDetails>
        </Accordion>
      </FormGroup>
      <FormGroup>
        <Accordion square expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
          <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
          <Typography>
            <Grid >
          
          <Typography component="div">
          <Grid component="label" container alignItems="center" spacing={1}>
            <Grid item><FormLabel component="legend">Conduct teeth intervection?</FormLabel></Grid>
            <Grid item>
            <Switch checked={state.teeth} onChange={handleSwitchChange} name="teeth" />
            </Grid>
          
          </Grid>
        </Typography>
      
          </Grid>

            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              
            </Typography>
          </AccordionDetails>
        </Accordion>
      </FormGroup>
      <FormGroup>
        <Accordion square expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
          <AccordionSummary aria-controls="panel3d-content" id="panel3d-header">
          <Typography>
            <Grid >
      
      
          <Typography component="div">
          <Grid component="label" container alignItems="center" spacing={1}>
          
            <Grid item><FormLabel component="legend">Is this culling selection?</FormLabel></Grid>
            <Grid item>
            <Switch checked={state.culling} onChange={handleSwitchChange} name="culling" />
            </Grid>
          
          </Grid>
        </Typography>
      
          </Grid>

            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
              sit amet blandit leo lobortis eget. Lorem ipsum dolor sit amet, consectetur adipiscing
              elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.
            </Typography>
          </AccordionDetails>
        </Accordion>
      </FormGroup>
     
      </FormControl>
        </div>
  );
}