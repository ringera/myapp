import React from 'react';

import './App.css';
import CustomizedAccordions from './accordion';
import CustomizedSwitches from './CustomizedSwitches';


function App() {
  return (
    <div className="App">
      <header className="App-header">
     <CustomizedAccordions />
    
      </header>
    </div>
  );
}

export default App;
