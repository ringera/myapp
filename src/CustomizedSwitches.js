import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { purple } from '@material-ui/core/colors';
import {FormGroup,FormControl,FormHelperText,FormLabel} from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';


export default function CustomizedSwitches() {
    const [state, setState] = React.useState({
        gilad: true,
        jason: false,
        antoine: true,
      });
    
      const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
      };
  return (
    <FormControl component="fieldset">
    <FormLabel component="legend">Assign responsibility</FormLabel>
    <FormGroup>
     
        <Typography component="div">
        <Grid component="label" container alignItems="center" spacing={1}>
          <Grid item>Move Animal</Grid>
          <Grid item>
          <Switch checked={state.gilad} onChange={handleChange} name="gilad" />
          </Grid>
        
        </Grid>
      </Typography>
     
      
    </FormGroup>
    <FormHelperText>Be careful</FormHelperText>
    
  
    </FormControl>
  );
}